//
//  PokemonCollectionViewCell.swift
//  PokeRadar
//
//  Created by Esteban Preciado on 3/16/19.
//  Copyright © 2019 Esteban Preciado. All rights reserved.
//

import UIKit

class PokemonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pokemonImageView: UIImageView!
    
    
    @IBOutlet weak var pokemonName: UILabel!
    
    @IBOutlet weak var pokemonIdLabel: UILabel!
    
}
