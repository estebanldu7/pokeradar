//
//  Pokemon.swift
//  PokeRadar
//
//  Created by Esteban Preciado on 3/15/19.
//  Copyright © 2019 Esteban Preciado. All rights reserved.
//

import Foundation
import UIKit

class Pokemon: NSObject {
    var id : Int
    var name: String
    var image: UIImage
    
    init(id: Int, name: String){
        self.id = id
        self.name = name
        self.image = UIImage(named: "\(id).png")!

    }
}
