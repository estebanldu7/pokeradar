//
//  ViewController.swift
//  PokeRadar
//
//  Created by Esteban Preciado on 3/13/19.
//  Copyright © 2019 Esteban Preciado. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase
import GeoFire

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    var mapCentered = false
    var geoFire: GeoFire!
    var geoFireRef : DatabaseReference!
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Firebase
        self.geoFireRef = Database.database().reference() // Referencia a la base de datos "Puntero"
        self.geoFire = GeoFire(firebaseRef: self.geoFireRef)
        // Do any additional setup after loading the view, typically from a nib.
        //Configuracion del mapa
        self.mapView.delegate = self
        self.mapView.userTrackingMode = .follow
        self.locationManager.delegate = self
        
        //Recieve notifies from PokedexCollectionViewController
        NotificationCenter.default.addObserver(self, selector: #selector(notify), name: NSNotification.Name(rawValue: "NotifyPokemon"), object: nil)
        locationAuthStatus()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //Permisos de localizacion
    func locationAuthStatus(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.mapView.showsUserLocation = true
        }else{
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    //Si el usuario le da permisos
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status ==  .authorizedWhenInUse{
            self.mapView.showsUserLocation = true
        }
    }
    
    func centerMap(on location: CLLocation){
        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        self.mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if !mapCentered {
            if let location = userLocation.location {
                centerMap(on: location)
                mapCentered = true
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView :  MKAnnotationView?
        
        let annotationIdentifier = "Pokemon"
        
        //Muestra y realiza la funcionalidad del entrenador ASH en el mapa
        if annotation.isKind(of: MKUserLocation.self){
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "User")
            annotationView?.image = UIImage(named: "character.png")
        }else if let dequeuedAnnotation = self.mapView.dequeueReusableAnnotationView(withIdentifier:    annotationIdentifier){
            annotationView = dequeuedAnnotation
            annotationView?.annotation = annotation
        }else{
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView, let pokemonAnnotation = annotation as? PokemonAnnotation {
            annotationView.canShowCallout = true
            annotationView.image = pokemonAnnotation.pokemon.image
            
            //Button
            let button = UIButton()
            button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            button.setImage(#imageLiteral(resourceName: "location"), for: .normal)
            annotationView.rightCalloutAccessoryView = button
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        let location = CLLocation(latitude: self.mapView.centerCoordinate.latitude, longitude: self.mapView.centerCoordinate.longitude)
        
         self.showSightingOnMap(on: location)
    }
    
    //Envia Datos hacia Firebase indicando el pokemon visto
    func createSighting(forLocation location: CLLocation, with PokemonId: Int){
        self.geoFire.setLocation(location, forKey: "\(PokemonId)")
    }
    
    //Muestra los pokemones vistos
    func showSightingOnMap(on location:CLLocation){
        //Contacta a FireBase a travez de GeoFIre y
        //devuelve todos los objetos guardados que estan a menos de 2km a la redonda
        let query = self.geoFire.query(at: location, withRadius: 2.0)
        
        query.observe(.keyEntered, with: { (key, location) in
            let annotation = PokemonAnnotation(coordinate: location.coordinate, pokemonId: Int(key)!)
            self.mapView.addAnnotation(annotation)
        })
    }
    
    //Permite abrir el mapa de IOS y muestra como llegar hasta el pokemon    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        //Se hace click en el boton de las anotaciones
        if let annotation = view.annotation as? PokemonAnnotation {
            let place = MKPlacemark(coordinate: annotation.coordinate)
            let destination = MKMapItem(placemark: place)
            destination.name = "\(annotation.pokemon.name) encontrado"
            
            let distance : CLLocationDistance = 1000 //1000 metros a la redonda
            let span = MKCoordinateRegion.init(center: annotation.coordinate, latitudinalMeters: distance, longitudinalMeters: distance)
            
            //Configuracion del mapa
            let option = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: span.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: span.span), MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving] as [String : Any]
            
            MKMapItem.openMaps(with: [destination], launchOptions: option)
        }
    }

//    @IBAction func reportPokemon(_ sender: UIButton) {
//        let location = CLLocation(latitude: self.mapView.centerCoordinate.latitude, longitude: self.mapView.centerCoordinate.longitude)
//        let pokemonIdRand = arc4random_uniform(151) + 1
//        self.createSighting(forLocation: location, with: Int(pokemonIdRand))
//    }
    
    @objc func notify(notif: Notification){
        //Notify to Firebase a Pokemon
        if let pokmeon = notif.object as? Pokemon {
            let location = CLLocation(latitude: self.mapView.centerCoordinate.latitude, longitude: self.mapView.centerCoordinate.longitude)
            let pokemonIdRand = arc4random_uniform(151) + 1
            
            self.createSighting(forLocation: location, with: pokmeon.id)
        }
    }
}

