//
//  PokemonAnnotation.swift
//  PokeRadar
//
//  Created by Esteban Preciado on 3/15/19.
//  Copyright © 2019 Esteban Preciado. All rights reserved.
//

import Foundation
import MapKit

class PokemonAnnotation: NSObject, MKAnnotation {
    var coordinate = CLLocationCoordinate2D()
    var pokemon : Pokemon
    var title: String?
    
    init(coordinate: CLLocationCoordinate2D, pokemonId: Int) {
        self.coordinate = coordinate
        self.pokemon = PokemonFactory.shared.getPokemon(with: pokemonId)
        self.title = self.pokemon.name
    }
}
